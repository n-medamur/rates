import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        rates: {}
    },
    mutations: {
        setRates(state, values) {
            state.rates = values
        }
    },
    actions: {
        async getRates({commit}) {
            const response = await fetch('https://api.exchangeratesapi.io/latest')
            const result = await response.json()
            commit('setRates', result)
        },
        async changeBase({commit}, name) {
            const response = await fetch(`https://api.exchangeratesapi.io/latest?base=${name}`)
            const result = await response.json()
            commit('setRates', result)
        }
    }
})
